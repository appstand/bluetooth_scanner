# Bluetooth Scanner

Allows for classic and low-energy bluetooth scanning.

## Getting Started

To use the automated version of the stream interface just call:
Stream<BluetoothScanResult> startClassicScan() for classic scan.
Or
Stream<BluetoothScanResult> startLeScan() for low-energy scan.

Both methods take care of permission handling and checking for the device' bluetooth being enabled internally.

More documentation on other methods and permission handling coming soon..