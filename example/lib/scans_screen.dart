import 'package:flutter/material.dart';
import 'package:flutter_plugin/bluetooth_device.dart';
import 'package:flutter_plugin/bluetooth_scan_result.dart';
import 'package:flutter_plugin/bluetooth_scanner.dart';

class ScansScreen extends StatefulWidget {
  final bool isClassic;
  final BluetoothScanner scanner;

  const ScansScreen({Key key, @required this.isClassic, @required this.scanner})
      : super(key: key);

  @override
  _ScansScreenState createState() => _ScansScreenState();
}

class _ScansScreenState extends State<ScansScreen> {
  Set<BluetoothDevice> devices = {};

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(title: Text("Bluetooth Scanner Example")),
          body: _devicesStreamBuilder()),
    );
  }

  Widget _devicesStreamBuilder() {
    return StreamBuilder(
        stream: widget.isClassic
            ? widget.scanner.startClassicScan()
            : widget.scanner.startLeScan(),
        builder: (BuildContext context,
            AsyncSnapshot<BluetoothScanResult> snapshot) {
          if (snapshot.hasData) {
            devices.add(snapshot.data.device);
            return ListView.separated(
                separatorBuilder: (context, index) =>
                    const Divider(color: Colors.black),
                itemCount: devices.length,
                itemBuilder: (BuildContext ctx, int index) {
                  final element = devices.elementAt(index);
                  return ListTile(
                    title: Text(element.name != null ? element.name : ""),
                    subtitle: Text(element.address),
                    leading: const Icon(Icons.bluetooth),
                  );
                });
          } else if (snapshot.hasError) {
            return Center(child: Text(snapshot.error.toString()));
          } else {
            return Center(child: Text("No devices found"));
          }
        });
  }
}
