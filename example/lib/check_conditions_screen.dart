import 'package:flutter/material.dart';
import 'package:flutter_plugin/bluetooth_scanner.dart';
import 'package:flutter_plugin_example/scans_screen.dart';

class CheckConditionsScreen extends StatefulWidget {
  @override
  _CheckConditionsScreenState createState() => _CheckConditionsScreenState();
}

class _CheckConditionsScreenState extends State<CheckConditionsScreen> {
  BluetoothScanner scanner = BluetoothScanner();
  bool hasPermissions = false;
  bool bluetoothEnabled = false;
  bool initialized = false;

  @override
  void initState() {
    scanner.hasPermissions.then((hasPermissions) {
      scanner.isEnabled.then((bluetoothEnabled) {
        setState(() {
          this.hasPermissions = hasPermissions;
          this.bluetoothEnabled = bluetoothEnabled;
          this.initialized = true;
        });
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Bluetooth Scanner Example")),
      body: Padding(
          padding: EdgeInsets.all(8.0), child: _checkConditions(context)),
    );
  }

  Widget _checkConditions(BuildContext context) {
    if (initialized) {
      if (!hasPermissions) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
                "Please verify that you have permissions. Click the button below to verify"),
            FlatButton(
                onPressed: () async {
                  bool hasPermissions = await scanner.openSettings();
                  setState(() {
                    this.hasPermissions = hasPermissions;
                  });
                },
                child: Text("Verify Permissions"))
          ],
        );
      } else if (!bluetoothEnabled) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
                "Please verify that bluetooth is switched on. Click the button below to turn it on."),
            FlatButton(
                onPressed: () async {
                  bool isBluetoothEnabled = await scanner.requestEnableBluetooth();
                  setState(() {
                    this.bluetoothEnabled = isBluetoothEnabled;
                  });
                },
                child: Text("Enable"))
          ],
        );
      } else {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            FlatButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ScansScreen(
                              isClassic: true,
                              scanner: scanner,
                            )),
                  );
                },
                child: Text("Start Classic Scan")),
            FlatButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ScansScreen(
                              isClassic: false,
                              scanner: scanner,
                            )),
                  );
                },
                child: Text("Start Low Energy Scan"))
          ],
        );
      }
    } else {
      return Container();
    }
  }
}
