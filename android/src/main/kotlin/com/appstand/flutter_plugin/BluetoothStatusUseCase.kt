package com.appstand.flutter_plugin

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.core.app.ActivityCompat
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.PluginRegistry
import io.flutter.view.FlutterNativeView

interface BluetoothStatusUseCase {
    var stateChannel: EventChannel?
    fun isOn(): Boolean
    fun enable(successCallback: SuccessCallback)
}

interface SuccessCallback {
    fun onResult(success: Boolean)
}

class BluetoothStatusUseCaseImpl : BluetoothStatusUseCase,
    PluginRegistry.ActivityResultListener,
    PluginRegistry.ViewDestroyListener {

    private val logTag = "StatusUseCase"
    private var pendingResultForActivityResult: SuccessCallback? = null
    private var binding: ActivityPluginBinding? = null
    private var bluetoothAdapter: BluetoothAdapter? = null
    var statusSink: EventChannel.EventSink? = null
    private val stateReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (statusSink == null) {
                return
            }

            when (intent.action) {
                BluetoothAdapter.ACTION_STATE_CHANGED -> {
                    statusSink?.success(
                        intent.getIntExtra(
                            BluetoothAdapter.EXTRA_STATE,
                            BluetoothDevice.ERROR
                        )
                    )
                }
            }
        }
    }
    private val stateStreamHandler: EventChannel.StreamHandler =
        object : EventChannel.StreamHandler {
            override fun onListen(arguments: Any?, events: EventChannel.EventSink?) {
                statusSink = events

                binding?.activity?.registerReceiver(
                    stateReceiver,
                    IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
                )
            }

            override fun onCancel(arguments: Any?) {
                statusSink = null
                try {
                    binding?.activity?.unregisterReceiver(stateReceiver)
                } catch (ex: java.lang.IllegalArgumentException) {
                }
            }

        }

    fun onAttachedToEngine(flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        stateChannel =
            EventChannel(
                flutterPluginBinding.binaryMessenger,
                "$pluginNamespace/state"
            )
        stateChannel?.setStreamHandler(stateStreamHandler)
    }

    fun onDetachedFromEngine() {
        this.bluetoothAdapter = null
        this.stateChannel?.setStreamHandler(null)
        this.stateChannel = null
    }

    override var stateChannel: EventChannel? = null

    override fun isOn(): Boolean {
        val bluetoothAdapter = this.bluetoothAdapter
        val isEnabled = bluetoothAdapter?.isEnabled ?: false
        return isEnabled
    }

    override fun enable(successCallback: SuccessCallback) {
        val adapter = bluetoothAdapter
        val binding = this.binding
        if (adapter != null && binding != null) {
            pendingResultForActivityResult = successCallback
            val intent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            ActivityCompat.startActivityForResult(
                binding.activity,
                intent,
                requestEnableBluetooth,
                null
            )
        } else {
            pendingResultForActivityResult?.onResult(true)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        return when (requestCode) {
            requestEnableBluetooth -> {
                pendingResultForActivityResult?.onResult(resultCode != Activity.RESULT_CANCELED)
                true
            }
            else -> false
        }
    }

    fun onAttachedToActivity(binding: ActivityPluginBinding) {
        this.binding = binding
        this.bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        this.binding?.addActivityResultListener(this)
    }

    fun onDetachedFromActivity() {
        binding?.removeActivityResultListener(this)
        this.bluetoothAdapter = null
        this.binding = null
    }

    override fun onViewDestroy(view: FlutterNativeView?): Boolean {
        try {
            binding?.activity?.unregisterReceiver(stateReceiver)
        } catch (ex: java.lang.IllegalArgumentException) {
        }
        return false
    }
}
