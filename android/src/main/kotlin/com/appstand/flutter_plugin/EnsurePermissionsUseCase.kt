package com.appstand.flutter_plugin

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.PluginRegistry


interface EnsurePermissionsUseCase {
    fun ensurePermissions(
        activity: Activity,
        callback: EnsurePermissionsCallback
    )

    fun openSettings(callback: EnsurePermissionsCallback)
}

interface EnsurePermissionsCallback {
    fun onResult(granted: Boolean)
}

class EnsurePermissionsUseCaseImpl : EnsurePermissionsUseCase,
    PluginRegistry.RequestPermissionsResultListener,
    PluginRegistry.ActivityResultListener {

    private var pendingPermissionsEnsureCallbacks: EnsurePermissionsCallback? = null
    private var binding: ActivityPluginBinding? = null
    private val requestCodeBluetoothPermissions = 2468
    private var ensurePermissionsCallback: EnsurePermissionsCallback? = null

    override fun ensurePermissions(
        activity: Activity,
        callback: EnsurePermissionsCallback
    ) {
        if (ContextCompat.checkSelfPermission(
                activity,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ),
                requestLocationPermissions
            )
            pendingPermissionsEnsureCallbacks = callback
        } else {
            callback.onResult(true)
        }
    }

    override fun openSettings(callback: EnsurePermissionsCallback) {
        this.ensurePermissionsCallback = callback
        val intent = Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri: Uri = Uri.fromParts("package", BuildConfig.LIBRARY_PACKAGE_NAME, null)
        intent.data = uri
        binding?.activity?.startActivityForResult(
            intent,
            requestCodeBluetoothPermissions
        )
    }

    fun onAttached(binding: ActivityPluginBinding) {
        this.binding = binding
        this.binding?.addActivityResultListener(this)
        this.binding?.addRequestPermissionsResultListener(this)
    }

    fun onDetached() {
        binding?.removeActivityResultListener(this)
        binding?.removeRequestPermissionsResultListener(this)
        this.binding = null
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>?,
        grantResults: IntArray?
    ): Boolean {
        when (requestCode) {
            requestLocationPermissions -> {
                pendingPermissionsEnsureCallbacks?.onResult(grantResults!![0] == PackageManager.PERMISSION_GRANTED)
                pendingPermissionsEnsureCallbacks = null
                return true
            }
        }
        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        if (requestCode == requestCodeBluetoothPermissions) {
            val activity = binding?.activity
            val callback = ensurePermissionsCallback
            if (activity != null && callback != null) {
                ensurePermissions(activity, callback)
            }
            return true
        }
        return false
    }
}
