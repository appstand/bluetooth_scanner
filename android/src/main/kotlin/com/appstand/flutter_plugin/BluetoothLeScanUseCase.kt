package com.appstand.flutter_plugin

import android.annotation.TargetApi
import android.bluetooth.BluetoothAdapter
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.os.Build
import android.util.Log
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.EventChannel


interface BluetoothLeScanUseCase {
    fun startScan()
    fun isScanning()
    fun stopScan()
}

class BluetoothLeScanUseCaseImpl : BluetoothLeScanUseCase {

    private val logTag = "LeScanUseCase"

    private var bluetoothAdapter: BluetoothAdapter? = null
    private var bluetoothLeScanner: BluetoothLeScanner? = null
    private var scanning = false
    private var leScansChannel: EventChannel? = null
    private var leResultSink: EventChannel.EventSink? = null
    private val leScansStreamHandler: EventChannel.StreamHandler = object :
        EventChannel.StreamHandler {

        override fun onListen(arguments: Any?, events: EventChannel.EventSink?) {
            leResultSink = events
        }

        override fun onCancel(arguments: Any?) {
            Log.d(logTag, "Canceling discovery (stream closed)")
            stopScan()
            leResultSink?.endOfStream()
            leResultSink = null
        }
    }

    @TargetApi(21)
    private val leScanCallback21: ScanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            super.onScanResult(callbackType, result)
            val device = result.device
            val scanResult: MutableMap<String, Any> =
                HashMap()
            if (device != null) {
                scanResult["address"] = device.address
                if (device.name != null) {
                    scanResult["name"] = device.name
                }
                scanResult["type"] = device.type
                scanResult["rssi"] = result.rssi
            }
            if (device != null) {
                Log.d(logTag, "Discovered " + device.address)
            }
            leResultSink?.success(scanResult)
        }
    }

    private val leScanCallback18: BluetoothAdapter.LeScanCallback =
        BluetoothAdapter.LeScanCallback { bluetoothDevice, rssi, _ ->
            val scanResult: MutableMap<String, Any> =
                HashMap()
            if (bluetoothDevice?.address != null) {
                scanResult["address"] = bluetoothDevice.address
                if (bluetoothDevice.name != null) {
                    scanResult["name"] = bluetoothDevice.name
                }
                scanResult["type"] = bluetoothDevice.type
                scanResult["rssi"] = rssi
                Log.d(logTag, "Discovered " + bluetoothDevice.address)
            }
            leResultSink?.success(scanResult)
        }

    fun onAttachedToEngine(flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        this.bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        leScansChannel = EventChannel(
            flutterPluginBinding.binaryMessenger,
            "$pluginNamespace/le_scan"
        )
        leScansChannel?.setStreamHandler(leScansStreamHandler)
    }

    fun onDetachedFromEngine() {
        if (scanning) {
            stopScan()
        }
        this.bluetoothLeScanner = null
        this.leScansChannel?.setStreamHandler(null)
        this.leScansChannel = null
    }

    override fun startScan() {
        if (!scanning) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                startScan21()
            } else {
                startScan18()
            }
            scanning = true
        }
    }

    override fun isScanning() {
        return isScanning()
    }

    override fun stopScan() {
        if (scanning) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                stopScan21()
            } else {
                stopScan18()
            }
            scanning = false
        }
    }

    @TargetApi(21)
    private fun startScan21() {
        this.bluetoothLeScanner = this.bluetoothAdapter?.bluetoothLeScanner
        bluetoothLeScanner?.startScan(leScanCallback21)
    }

    @Suppress("DEPRECATION")
    private fun startScan18() {
        bluetoothAdapter?.startLeScan(leScanCallback18)
    }

    @TargetApi(21)
    private fun stopScan21() {
        bluetoothLeScanner?.stopScan(leScanCallback21)
    }

    @Suppress("DEPRECATION")
    private fun stopScan18() {
        bluetoothAdapter?.stopLeScan(leScanCallback18)
    }
}
