package com.appstand.flutter_plugin

import android.bluetooth.BluetoothAdapter
import android.util.Log
import androidx.annotation.NonNull
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result

/** Scanner plugin for Classic and Low Energy Bluetooth devices */
class BluetoothScanner : FlutterPlugin,
    MethodCallHandler,
    ActivityAware {

    private val logTag = "bluetooth_scanner"

    // could be injected at some point if ever opt for writing tests
    private val bluetoothClassicScanUseCase: BluetoothClassicScanUseCase =
        BluetoothClassicScanUseCaseImpl()
    private val ensurePermissionsUseCase: EnsurePermissionsUseCase = EnsurePermissionsUseCaseImpl()
    private val bluetoothStatusUseCase: BluetoothStatusUseCase = BluetoothStatusUseCaseImpl()
    private val bluetoothLeScanUseCase: BluetoothLeScanUseCase = BluetoothLeScanUseCaseImpl()

    private var pluginBinding: ActivityPluginBinding? = null

    private lateinit var channel: MethodChannel
    private var bluetoothAdapter: BluetoothAdapter? = null

    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        channel = MethodChannel(flutterPluginBinding.binaryMessenger, pluginNamespace)
        channel.setMethodCallHandler(this)
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        (bluetoothClassicScanUseCase as BluetoothClassicScanUseCaseImpl).onAttachedToEngine(
            flutterPluginBinding
        )
        (bluetoothStatusUseCase as BluetoothStatusUseCaseImpl).onAttachedToEngine(
            flutterPluginBinding
        )
        (bluetoothLeScanUseCase as BluetoothLeScanUseCaseImpl).onAttachedToEngine(
            flutterPluginBinding
        )
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        Log.d(logTag, call.method)
        when (call.method) {
            "ensure_permissions" -> {
                val pluginBinding = this.pluginBinding
                if (pluginBinding != null) {
                    ensurePermissionsUseCase.ensurePermissions(
                        pluginBinding.activity,
                        object : EnsurePermissionsCallback {
                            override fun onResult(granted: Boolean) {
                                Log.d(logTag, "Permissions: $granted")
                                result.success(granted)
                            }
                        })
                } else {
                    result.success(false)
                }
            }
            "open_settings" -> {
                val binding = pluginBinding
                if (binding != null) {
                    ensurePermissionsUseCase.openSettings(object : EnsurePermissionsCallback {
                        override fun onResult(granted: Boolean) {
                            result.success(granted)
                        }

                    })
                } else {
                    result.success(false)
                }
            }
            "is_enabled" -> {
                result.success(bluetoothStatusUseCase.isOn())
            }
            "enable" -> {
                bluetoothStatusUseCase.enable(object : SuccessCallback {
                    override fun onResult(success: Boolean) {
                        result.success(success)
                    }
                })
            }
            "start_classic_scan" -> {
                val pluginBinding = this.pluginBinding
                if (pluginBinding != null) {
                    ensurePermissionsUseCase.ensurePermissions(
                        pluginBinding.activity,
                        object : EnsurePermissionsCallback {
                            override fun onResult(granted: Boolean) {
                                if (!granted) {
                                    result.error(
                                        noPermissionsErrorCode,
                                        noPermissionsErrorCode,
                                        null
                                    )
                                    return
                                }
                                if (bluetoothStatusUseCase.isOn()) {
                                    bluetoothClassicScanUseCase.startDiscovery()
                                    result.success(null)
                                } else {
                                    result.error(
                                        bluetoothDisabledErrorCode,
                                        bluetoothDisabledMessage,
                                        null
                                    )
                                }
                            }
                        })
                } else {
                    result.error(
                        noPermissionsErrorCode,
                        noPermissionsErrorCode,
                        null
                    )
                }
            }
            "stop_classic_scan" -> {
                bluetoothClassicScanUseCase.cancelDiscovery()
                result.success(null)
            }
            "classic_scan_running" -> result.success(bluetoothClassicScanUseCase.isDiscovering())
            "start_le_scan" -> {
                val binding = this.pluginBinding
                if (binding != null) {
                    ensurePermissionsUseCase.ensurePermissions(
                        binding.activity,
                        object : EnsurePermissionsCallback {
                            override fun onResult(granted: Boolean) {
                                if (!granted) {
                                    result.error(
                                        noPermissionsErrorCode,
                                        noPermissionsMessage,
                                        null
                                    )
                                    return
                                }
                                if (bluetoothStatusUseCase.isOn()) {
                                    bluetoothLeScanUseCase.startScan()
                                    result.success(null)
                                } else {
                                    result.error(
                                        bluetoothDisabledErrorCode,
                                        bluetoothDisabledMessage,
                                        null
                                    )
                                }
                            }
                        })
                } else {
                    result.error(
                        noPermissionsErrorCode,
                        noPermissionsMessage,
                        null
                    )
                }
            }
            "stop_le_scan" -> {
                bluetoothLeScanUseCase.stopScan()
                result.success(null)
            }
            "le_scan_running" -> {
                result.success(bluetoothLeScanUseCase.isScanning())
            }
            else -> {
                result.notImplemented()
            }
        }
    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        channel.setMethodCallHandler(null)

        (bluetoothClassicScanUseCase as BluetoothClassicScanUseCaseImpl).onDetachedFromEngine()
        (bluetoothStatusUseCase as BluetoothStatusUseCaseImpl).onDetachedFromEngine()
        (bluetoothLeScanUseCase as BluetoothLeScanUseCaseImpl).onDetachedFromEngine()
        this.bluetoothAdapter = null
    }

    override fun onDetachedFromActivity() {
        Log.d(logTag, "detachedActivity")
        this.pluginBinding = null
        (ensurePermissionsUseCase as EnsurePermissionsUseCaseImpl).onDetached()
        val bluetoothClassicScanUseCase =
            (this.bluetoothClassicScanUseCase as BluetoothClassicScanUseCaseImpl)
        bluetoothClassicScanUseCase.onDetachedFromActivity()
        val bluetoothStatusUseCase =
            (this.bluetoothStatusUseCase as BluetoothStatusUseCaseImpl)
        bluetoothStatusUseCase.onDetachedFromActivity()
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
        Log.d(logTag, "reAttachedActivity")
        this.pluginBinding = binding
        (ensurePermissionsUseCase as EnsurePermissionsUseCaseImpl).onAttached(binding)
        val bluetoothClassicScanUseCase =
            (this.bluetoothClassicScanUseCase as BluetoothClassicScanUseCaseImpl)
        bluetoothClassicScanUseCase.onAttachedToActivity(binding)
        val bluetoothStatusUseCase =
            (this.bluetoothStatusUseCase as BluetoothStatusUseCaseImpl)
        bluetoothStatusUseCase.onAttachedToActivity(binding)
    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        Log.d(logTag, "onAttachedActivity")
        this.pluginBinding = binding
        (ensurePermissionsUseCase as EnsurePermissionsUseCaseImpl).onAttached(binding)
        val bluetoothClassicScanUseCase =
            (this.bluetoothClassicScanUseCase as BluetoothClassicScanUseCaseImpl)
        bluetoothClassicScanUseCase.onAttachedToActivity(binding)
        val bluetoothStatusUseCase =
            (this.bluetoothStatusUseCase as BluetoothStatusUseCaseImpl)
        bluetoothStatusUseCase.onAttachedToActivity(binding)
    }

    override fun onDetachedFromActivityForConfigChanges() {
        Log.d(logTag, "deAttachedConfig")
        this.pluginBinding = null
        (ensurePermissionsUseCase as EnsurePermissionsUseCaseImpl).onDetached()
        val bluetoothClassicScanUseCase =
            (this.bluetoothClassicScanUseCase as BluetoothClassicScanUseCaseImpl)
        bluetoothClassicScanUseCase.onDetachedFromActivity()
        val bluetoothStatusUseCase =
            (this.bluetoothStatusUseCase as BluetoothStatusUseCaseImpl)
        bluetoothStatusUseCase.onDetachedFromActivity()
    }
}
