package com.appstand.flutter_plugin

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.util.Log
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.PluginRegistry
import io.flutter.view.FlutterNativeView

/***
 * Interface for methods to be used when it is sure that the application has permissions to
 * do classic Bluetooth scanning
 */
interface BluetoothClassicScanUseCase {
    fun startDiscovery()
    fun isDiscovering(): Boolean
    fun cancelDiscovery()
}

class BluetoothClassicScanUseCaseImpl : BluetoothClassicScanUseCase,
    PluginRegistry.ViewDestroyListener {

    private val logTag = "ClassicScanUseCase"

    private var pluginBinding: ActivityPluginBinding? = null
    private var bluetoothAdapter: BluetoothAdapter? = null
    private var discoverySink: EventChannel.EventSink? = null
    private var discoveryChannel: EventChannel? = null
    private val discoveryStreamHandler: EventChannel.StreamHandler = object :
        EventChannel.StreamHandler {

        override fun onListen(arguments: Any?, events: EventChannel.EventSink?) {
            discoverySink = events
        }

        override fun onCancel(arguments: Any?) {
            Log.d(logTag, "Canceling discovery (stream closed)")
            pluginBinding?.activity?.unregisterReceiver(discoveryReceiver)
            bluetoothAdapter?.cancelDiscovery()
            discoverySink?.endOfStream()
            discoverySink = null
        }
    }
    private val discoveryReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.action) {
                BluetoothDevice.ACTION_FOUND -> {
                    val device =
                        intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                    val deviceRSSI = intent.getShortExtra(
                        BluetoothDevice.EXTRA_RSSI,
                        Short.MIN_VALUE
                    ).toInt()
                    val discoveryResult: MutableMap<String, Any> =
                        HashMap()
                    if (device != null) {
                        discoveryResult["address"] = device.address
                        if (device.name != null) {
                            discoveryResult["name"] = device.name
                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                            discoveryResult["type"] = device.type
                        }
                        discoveryResult["rssi"] = deviceRSSI
                    }
                    if (device != null) {
                        Log.d(logTag, "Discovered " + device.address)
                    }
                    discoverySink?.success(discoveryResult)
                }
                BluetoothAdapter.ACTION_DISCOVERY_FINISHED -> {
                    Log.d(logTag, "Discovery round finished")
                    bluetoothAdapter?.startDiscovery()
                }
                else -> {
                }
            }
        }
    }

    override fun startDiscovery() {
        Log.d(logTag, "Starting discovery")
        val intent = IntentFilter()
        intent.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)
        intent.addAction(BluetoothDevice.ACTION_FOUND)
        pluginBinding?.activity?.registerReceiver(discoveryReceiver, intent)
        bluetoothAdapter?.startDiscovery()
    }

    override fun isDiscovering(): Boolean {
        return bluetoothAdapter?.isDiscovering ?: false
    }

    override fun cancelDiscovery() {
        Log.d(logTag, "Canceling discovery")
        try {
            pluginBinding?.activity?.unregisterReceiver(discoveryReceiver)
        } catch (ex: java.lang.IllegalArgumentException) {
        }
        bluetoothAdapter?.cancelDiscovery()

        discoverySink?.endOfStream()
        discoverySink = null
    }

    fun onAttachedToEngine(flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        this.bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        discoveryChannel = EventChannel(
            flutterPluginBinding.binaryMessenger,
            "$pluginNamespace/classic_scan"
        )
        discoveryChannel?.setStreamHandler(discoveryStreamHandler)
    }

    fun onDetachedFromEngine() {
        this.bluetoothAdapter = null
        this.discoveryChannel?.setStreamHandler(null)
        this.discoveryChannel = null
    }

    fun onAttachedToActivity(binding: ActivityPluginBinding) {
        this.pluginBinding = binding
    }

    fun onDetachedFromActivity() {
        this.pluginBinding = null
    }

    override fun onViewDestroy(view: FlutterNativeView?): Boolean {
        try {
            pluginBinding?.activity?.unregisterReceiver(discoveryReceiver)
        } catch (ex: IllegalArgumentException) {
        }
        return false
    }
}
