package com.appstand.flutter_plugin

const val pluginNamespace = "flutter_bluetooth_scanner"
const val requestLocationPermissions = 5678
const val requestEnableBluetooth = 1234

const val noPermissionsErrorCode = "no_permissions"
const val noPermissionsMessage = "Scanning for devices requires location access permission"
const val bluetoothDisabledErrorCode = "bluetooth_disabled"
const val bluetoothDisabledMessage = "Scanning for devices required bluetooth to be enabled"
