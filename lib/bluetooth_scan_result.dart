import 'bluetooth_device.dart';

class BluetoothScanResult {
  final BluetoothDevice device;
  final int rssi;

  BluetoothScanResult({
    this.device,
    this.rssi = 0,
  });

  factory BluetoothScanResult.fromMap(Map map) {
    return BluetoothScanResult(
      device: BluetoothDevice.fromMap(map),
      rssi: map['rssi'] ?? 0,
    );
  }
}
