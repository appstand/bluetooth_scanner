import 'bluetooth_device_type.dart';

class BluetoothDevice {
  final String name;
  final String address;
  final BluetoothDeviceType type;

  const BluetoothDevice(
      {this.name, this.address, this.type = BluetoothDeviceType.unknown});

  factory BluetoothDevice.fromMap(Map map) {
    return BluetoothDevice(
        name: map["name"],
        address: map["address"],
        type: map["type"] != null
            ? BluetoothDeviceType.fromUnderlyingValue(map["type"])
            : BluetoothDeviceType.unknown);
  }

  Map<String, dynamic> toMap() => {
        "name": this.name,
        "address": this.address,
        "type": this.type.toUnderlyingValue()
      };

  operator ==(Object other) {
    return other is BluetoothDevice && other.address == this.address;
  }

  @override
  int get hashCode => address.hashCode;
}
