import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter_plugin/bluetooth_scan_result.dart';

class BluetoothScanner {
  static const String _nameSpace = 'flutter_bluetooth_scanner';

  /// *
  /// Channel for sending commands to the target device
  static const MethodChannel _channel = const MethodChannel(_nameSpace);

  /// *
  /// Channel for receiving classic bluetooth scan results
  static final EventChannel _classicScanChannel =
      const EventChannel('$_nameSpace/classic_scan');

  /// *
  /// Channel for receiving low-energy bluetooth scan results
  static final EventChannel _leScanChannel =
      const EventChannel('$_nameSpace/le_scan');

  /// *
  /// Main public method for starting classic bluetooth scan.
  /// Does permission handling and then checks if bluetooth is enabled.
  /// If all is good then initiates a stream which returns a BluetoothScanResult
  ///
  Stream<BluetoothScanResult> startClassicScan() async* {
    yield* _checkPermissions(_startClassicScan());
  }

  /// *
  /// Main public method for starting low-energy bluetooth scan.
  /// Does permission handling and then checks if bluetooth is enabled.
  /// If all is good then initiates a stream which returns a BluetoothScanResult
  ///
  Stream<BluetoothScanResult> startLeScan() async* {
    yield* _checkPermissions(_startLeScan());
  }

  /// *
  /// Possibility to check for permissions yourself
  Future<bool> get hasPermissions async =>
      await _channel.invokeMethod<bool>('ensure_permissions');

  /// *
  /// Open settings for requesting permissions. Should be used when the user
  /// has declined permissions.
  Future<bool> openSettings() async =>
      await _channel.invokeMethod("open_settings");

  /// *
  /// Future which checks if the bluetooth is enabled on the target device
  Future<bool> get isEnabled async => await _channel.invokeMethod('is_enabled');

  /// *
  /// Asks the user to enable bluetooth on the target device
  Future<bool> requestEnableBluetooth() async =>
      await _channel.invokeMethod('enable');

  /// *
  /// Checks if Bluetooth Classic scanning is ongoing
  Future<bool> get isClassicScanRunning async =>
      await _channel.invokeMethod('classic_scan_running');

  /// *
  /// Stops Bluetooth classic scan on the target device
  Future<void> stopClassicScan() async =>
      await _channel.invokeMethod('stop_classic_scan');

  /// *
  /// Checks if Bluetooth Low-Energy scanning is ongoing
  Future<bool> get isLeScanRunning async =>
      await _channel.invokeMethod('le_scan_running');

  /// *
  /// Stops Bluetooth Low-Energy scan on the target device
  Future<void> stopLeScan() async =>
      await _channel.invokeMethod('stop_le_scan');

  Stream<BluetoothScanResult> _checkPermissions(Stream function) async* {
    bool hasPermissions = await this.hasPermissions;
    if (hasPermissions) {
      bool isBluetoothOn = await this.isEnabled;
      if (isBluetoothOn) {
        yield* function;
      } else {
        bool requestToTurnOnSuccess = await requestEnableBluetooth();
        if (requestToTurnOnSuccess) {
          yield* function;
        } else {
          throw Exception("User refused to turn on Bluetooth");
        }
      }
    } else {
      throw Exception("Bluetooth Scanner can not work without permissions!");
    }
  }

  Stream<BluetoothScanResult> _startClassicScan() async* {
    StreamSubscription subscription;
    StreamController controller;

    controller = new StreamController(
      onCancel: () {
        subscription?.cancel();
      },
    );

    await _channel.invokeMethod('start_classic_scan');

    subscription = _classicScanChannel.receiveBroadcastStream().listen(
          controller.add,
          onError: controller.addError,
          onDone: controller.close,
        );

    yield* controller.stream.map((map) => BluetoothScanResult.fromMap(map));
  }

  Stream<BluetoothScanResult> _startLeScan() async* {
    StreamSubscription subscription;
    StreamController controller;

    controller = new StreamController(
      onCancel: () {
        subscription?.cancel();
      },
    );

    await _channel.invokeMethod('start_le_scan');

    subscription = _leScanChannel.receiveBroadcastStream().listen(
          controller.add,
          onError: controller.addError,
          onDone: controller.close,
        );

    yield* controller.stream.map((map) => BluetoothScanResult.fromMap(map));
  }
}
